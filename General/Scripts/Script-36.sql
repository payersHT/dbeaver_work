SELECT rh.user_id
            ,TRUNC(rh.createdate) AS create_date
            ,rd.allocation_nbr
            ,SUBSTR (rd.UNIQUE_MER_KEY, 1, INSTR(rd.UNIQUE_MER_KEY, '.', -1, 1) -1) AS style_number
            ,SUBSTR (rd.UNIQUE_MER_KEY, INSTR(rd.UNIQUE_MER_KEY, '.', -1, 1) +1) AS sku_number
            ,rd.wl_key
            ,rd.result_qty
            ,wl.alloc_nbr
            ,wl.po_nbr
            ,wl.itm_nbr
            FROM aam.RESULTS_DETAIL rd
            JOIN aam.results_header rh
            ON rd.allocation_nbr = rh.allocation_nbr
			JOIN aam.WORKLIST wl
			ON wl.alloc_nbr = rd.ALLOCATION_NBR 
			JOIN 