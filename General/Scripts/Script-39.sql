SELECT
date_format(CONVERT_TZ(CTN.SHIPPED_DATE_TIME,  'UTC', 'US/Pacific'), '%m/%d/%Y %k:%i:%s') "Shipped Date", 
SCF.SHIP_CONFIRM_BATCH_ID "Ship Confirm Batch ID", 
SCFD.SHIP_CONFIRM_BATCH_DETAIL_ID "Ship Confirm Batch Detail ID",
SCFBDLPN.MSHIPMENT "Shipment ID",
CTN.DESTINATION_FACILITY_ID Destination,
concat('999',substr(SHIP_CONFIRM_BATCH_DETAIL_ID,10,6),substr(SHIP_CONFIRM_BATCH_DETAIL_ID,17)) ASN,
OLI.ALLOCATION_SOURCE_ID "PO", 
SCFBDLPN.MOLPN "oLPN", 
SCFBDLPN.MITEM "Item", SCFBDLPN.MPACKED "Packed Qty."
FROM  default_shipconfirm.SHC_SHIP_CONFIRM_BATCH SCF 
LEFT OUTER JOIN default_shipconfirm.SHC_SHIP_CONFIRM_BATCH_DETAIL SCFD ON SCFD.SHIP_CONFIRM_BATCH_ID = SCF.SHIP_CONFIRM_BATCH_ID
JOIN JSON_TABLE
(SCFD.ENRICHED_PAYLOAD, 
                '$'
                 COLUMNS (
                   ROWID FOR ORDINALITY,
                   NESTED PATH '$.Message.ShipConfirm.ShipConfirmDetails.OriginalOrder[*].Olpn[*]' 
                                        COLUMNS ( MOLPN       VARCHAR(50) PATH '$.OlpnId',
						  						  MSHIPMENT   VARCHAR(50) PATH '$.ShipmentId',
						  						  MLPNQTY     DECIMAL(10,2) PATH '$.TotalLpnQty',
                                                  MORDID      VARCHAR(50) PATH  '$.OlpnDetail[*].OrderId',
                                                  MORDLNID    VARCHAR(50) PATH  '$.OlpnDetail[*].OrderLineId',
                                                  MITEM       VARCHAR(10) PATH  '$.OlpnDetail[*].ItemId',
                                                  MPACKED     DECIMAL(10,2) PATH '$.OlpnDetail[*].PackedQuantity'
												)     
                   ) 
				) AS SCFBDLPN 
JOIN default_pickpack.PPK_OLPN CTN ON CTN.OLPN_ID = SCFBDLPN.MOLPN AND CTN.SHIPMENT_ID =  SCFBDLPN.MSHIPMENT
JOIN default_pickpack.PPK_OLPN_DETAIL CTND ON  CTN.PK = CTND.OLPN_PK  
JOIN default_dcorder.DCO_ORDER_LINE OLI ON CTND.ORDER_ID = OLI.ORDER_ID AND CTND.ORDER_LINE_ID = OLI.ORDER_LINE_ID 
/*AND  OLI.ORDER_ID = SCFBDLPN.MORDID*/ AND OLI.ORDER_LINE_ID = SCFBDLPN.MORDLNID 
WHERE SCF.SHIP_CONFIRM_TYPE_ID = 'CLOSE_SHIPMENT' 
AND CTN.STATUS = '8000' 
AND SCF.SHIPMENT_ID IN ('SHI000000128')
/*AND CTN.DESTINATION_FACILITY_ID  IN (997,1997) 
*/
/*AND CTN.DESTINATION_FACILITY_ID NOT IN (4597,4599,5990,5991,5997,5999,8100,4490,4491,4497,4499,4998) 
*/
/*AND date_format(CONVERT_TZ(CTN.SHIPPED_DATE_TIME,  'UTC', 'US/Pacific'), '%m/%d/%Y') >= date_format(str_to_date( '09/24/2021' , '%m/%d/%Y'),'%m/%d/%Y')
AND date_format(CONVERT_TZ(CTN.SHIPPED_DATE_TIME,  'UTC', 'US/Pacific'), '%m/%d/%Y') <= date_format(str_to_date( '10/05/2021' , '%m/%d/%Y'),'%m/%d/%Y')
*/
ORDER BY "Shipped Date", "Shipment ID", "Ship Confirm Batch Detail ID","PO", "oLPN", "Item";

                                    