SELECT wp.Itm_nbr,
      wp.Pack_ID,
      wp.Sku_Num,
      wp.Store_nbr,
      wp.Alloc_nbr,
      pk.Size_nbr,
      SUM(alloc)    AS alloc ,
      SUM(Wip)      AS WIP,
      SUM(inner_pk) AS Inner_pk
    FROM
      (SELECT NVL(rms.itm_nbr,wp.itm_nbr) AS itm_nbr ,
        wp.sku_num                        AS Pack_Id,
        wp.Store_nbr,
        wp.alloc_nbr,
        NVL(RMS.Sku_number,wp.sku_num) AS Sku_num ,
        SUM(wp.alloc_qty)              AS alloc,
        SUM(wp.WIP_OH_QTY)             AS WIP,
        SUM(Qty_per_pack)              AS inner_pk
      FROM aam.HT_WIP_STAGE WP
      LEFT OUTER JOIN aam.HT_Base_rms_packs RMS
      ON RMS.pack_id = wp.sku_num
       WHERE wp.wip_oh_qty > 0
    AND wp.itm_nbr = '10844665'
      GROUP BY NVL(rms.itm_nbr,wp.itm_nbr),
        wp.sku_num,
        wp.Store_nbr,
        wp.alloc_nbr,
        NVL(RMS.Sku_number,wp.sku_num)
      ) wp
    LEFT OUTER JOIN
      (SELECT sku_nbr, Size_nbr FROM aam.WL_pack GROUP BY sku_nbr, Size_nbr
      ) Pk
    ON wp.sku_num = pk.sku_nbr
    GROUP BY wp.itm_nbr,
      wp.pack_ID,
      wp.Sku_Num,
      wp.Store_nbr,
      wp.Alloc_nbr,
      pk.Size_nbr,
      wp.sku_num