SELECT w.ALLOC_NBR,
w.alloc_user,
w.WL_KEY,
w.STATUS_DESCRIPTION,
w.UPDATE_STATUS_DATE,
w.WAREHOUSE_NBR,
w.ITM_NBR ,
w.ITM_DESC1,
CASE WHEN TO_NUMBER( rd.LOCATION_ID) IN (4499,4490) THEN 'WEB Store'
WHEN TO_NUMBER( rd.LOCATION_ID) IN (9999,9997) THEN 'Backstock'
ELSE 'Store_num' END ,
TO_NUMBER( rd.LOCATION_ID),
rd.RESULT_QTY
FROM AAM.WORKLIST w 
INNER JOIN (SELECT ALLOC_NBR,max(UPDATE_STATUS_DATE) AS Max_date
			FROM AAM.WORKLIST
			GROUP BY ALLOC_NBR) b
ON b.Max_date = w.UPDATE_STATUS_DATE 
AND b.ALLOC_NBR = w.ALLOC_NBR 
INNER JOIN AAM.RESULTS_DETAIL rd 
ON rd.ALLOCATION_NBR = w.ALLOC_NBR 
WHERE w.ALLOC_NBR NOT IN ('0')
AND w.STATUS_DESCRIPTION = 'Released'
AND w.DIV_NBR ='1'
AND UPDATE_STATUS_DATE > TO_DATE('2022-02-15','YYYY-mm-DD') 
