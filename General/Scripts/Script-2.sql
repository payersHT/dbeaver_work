Select case when  E.D_State_Prov = ' ' then 'Unknown' else isnull(E.D_State_Prov,'Unknown') end as State,
		isnull(E.D_Country_Code,'Unknown') as CountryCode , isnull(E.D_Postal_code,'Unknown') as ZipCode, 
		count(distinct h.fullorderno) as OrderCnt, sum(Ext_PRC) as Sales
  		from dbo.Fact_ChannelDemandHdr H -- on E.TC_PURCHASE_ORDERS_ID = H.FULLORDERNO
Join dbo.Fact_ChannelDemandDtl D on H.FULLORDERNO = D.FULLORDERNO
left outer join  [ChannelDM].dbo.HTEOMOrders E
  on  H.ORDERNO = rtrim(case when E.TC_PURCHASE_ORDERS_ID like 'P%0000' or E.TC_PURCHASE_ORDERS_ID like 'X%0000' then substring(E.TC_PURCHASE_ORDERS_ID,1,8) 
 else E.TC_PURCHASE_ORDERS_ID end) 
 Join DimTime T on D.ENTRYDATE = T.DT
join DimSku S on D.SKU_NUM = S.SKU_NUM
where D.ENTRYDATE between '02-FEB-20' and '08-FEB-20'
  and LN_TP in ('RET','SAL')
  and H.DIVISION = @pdivision and Channel_Key not in ('10')
and S.DIV_CD = @pdivision
  and  cast( S.DEPT_CD  as varchar) like (case when @pDept='0' then '%' else @pDept end) 
  and S.CLASS_CD  like (case when @pClass='0' then '%' else @pClass end)
group by  case when  E.D_State_Prov = ' ' then 'Unknown' else isnull(E.D_State_Prov,'Unknown') end, isnull(E.D_Country_Code,'Unknown'),isnull(E.D_Postal_code,'Unknown')
