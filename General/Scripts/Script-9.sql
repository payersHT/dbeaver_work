WITH rd AS (SELECT
             --rh.user_id
            TRUNC(rh.createdate) AS create_date
            ,rd.allocation_nbr
            ,loc.location_id AS store_number
            ,SUBSTR (rd.UNIQUE_MER_KEY, 1, INSTR(rd.UNIQUE_MER_KEY, '.', -1, 1) -1) AS style_number
            ,SUBSTR (rd.UNIQUE_MER_KEY, INSTR(rd.UNIQUE_MER_KEY, '.', -1, 1) +1) AS pack_id
            ,rd.wl_key
            ,rd.result_qty
            FROM aam.RESULTS_DETAIL rd
            JOIN aam.results_header rh
            ON rd.allocation_nbr = rh.allocation_nbr
            JOIN aam.worklist wl
            ON rd.wl_key = wl.wl_key
            and wl.alloc_nbr = rd.allocation_nbr
            JOIN aam.locations loc
            ON rd.location_id = loc.store
            WHERE TRUNC(rh.createdate) = TRUNC(SYSDATE - 2)
            AND wl.div_nbr = 1
            AND wl.STATUS_DESCRIPTION IN ('Approved','Released')
)
SELECT rd.ALLOCATION_NBR
            --,rd.user_id
            ,rd.create_date
            ,rd.store_number
            ,rd.style_number
            ,pack.sku_nbr
            ,pack.size_nbr
            ,rd.WL_KEY
            ,rd.result_qty * NVL(pack.qty_per_pack, 1) AS result_qty
            ,CASE WHEN wl.po_nbr = 0 THEN rd.result_qty * NVL(pack.qty_per_pack, 1) ELSE 0 END AS backorder_qty
            ,CASE WHEN wl.po_nbr > 0 THEN rd.result_qty * NVL(pack.qty_per_pack, 1) ELSE 0 END AS po_qty
            ,rd.result_qty AS pack_results
	FROM rd rd
        JOIN aam.worklist wl
        	ON rd.wl_key = wl.wl_key
        JOIN aam.wl_pack pack
       		ON rd.wl_key = pack.wl_key
            AND rd.pack_id = pack.pack_id
  WHERE STYLE_NUMBER IN ('10497129')