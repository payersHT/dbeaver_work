select po.TC_COMPANY_ID,
po.TC_PURCHASE_ORDERS_ID, 
(select description 
	from PURCHASE_ORDERS_STATUS where PURCHASE_ORDERS_STATUS=po.PURCHASE_ORDERS_STATUS)PURCHASE_ORDERS_STATUS, 
po.BUSINESS_PARTNER_ID,po.D_FACILITY_ALIAS_ID, 
(select attribute_value 
from purchase_orders_attribute 
where purchase_orders_id=po.purchase_orders_id and attribute_name='PO_Type')PO_Type, 
po.DELIVERY_START_DTTM Start_Ship_Date, 
po.DELIVERY_END_DTTM Cancel_Date, 
poli.sku,DESCRIPTION, 
ORDER_QTY 
from 
purchase_orders po 
inner join PURCHASE_ORDERS_LINE_ITEM poli on poli.PURCHASE_ORDERS_ID=PO.PURCHASE_ORDERS_ID 
where po.tc_company_id=42 
and po.PURCHASE_ORDERS_STATUS=130 
and po.DELIVERY_END_DTTM between to_date('12/01/2021','MM/DD/YYYY') and to_date('12/31/2021','MM/DD/YYYY') 
and not exists ( 
select 1 from PURCHASE_ORDERS_EVENT POE where POE.PURCHASE_ORDERS_ID=PO.PURCHASE_ORDERS_ID AND POE.FIELD_NAME='STATUS' and POE.new_value = 'Pending Vendor Acceptance' 
)