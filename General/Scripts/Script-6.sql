WITH rd AS (SELECT rh.user_id
            ,TRUNC(rh.createdate) AS create_date
            ,rd.allocation_nbr
            ,loc.location_id AS store_number
            ,SUBSTR (rd.UNIQUE_MER_KEY, 1, INSTR(rd.UNIQUE_MER_KEY, '.', -1, 1) -1) AS style_number
            ,SUBSTR (rd.UNIQUE_MER_KEY, INSTR(rd.UNIQUE_MER_KEY, '.', -1, 1) +1) AS pack_id
            ,rd.wl_key
            ,rd.result_qty
            FROM aam.RESULTS_DETAIL rd
            JOIN aam.results_header rh
            ON rd.allocation_nbr = rh.allocation_nbr
            JOIN aam.worklist wl
            ON rd.wl_key = wl.wl_key
            and wl.alloc_nbr = rd.allocation_nbr
            JOIN aam.locations loc
            ON rd.location_id = loc.store
            WHERE TRUNC(rh.createdate) = TRUNC( SYSDATE - 2 )
            AND wl.div_nbr = 1
            AND wl.STATUS_DESCRIPTION IN ('Approved','Released')
)
SELECT
	--ar.user_id
	ar.create_date
	,ar.store_number
	,ar.style_number
	,ar.size_nbr
	,ar.model_key
	,(ar.result_qty) AS result_qty
    ,(ar.pack_results) AS pack_results
    ,(ar.backorder_qty) AS Backorder_qty
    ,(ar.po_qty) PO_qty
    --,ar.WL_KEY
	,md.threshold
	,md.target
FROM (SELECT 
        ar.user_id
        ,ar.create_date
        ,ar.store_number
        ,ar.style_number
        ,ar.size_nbr
        ,ar.WL_KEY
        ,rs.model_key
        ,SUM(ar.result_qty) AS result_qty
        ,SUM(pack_results) AS pack_results
        ,SUM(ar.backorder_qty) AS backorder_qty
        ,SUM(ar.po_qty) AS po_qty
    FROM (SELECT rd.ALLOCATION_NBR
            ,rd.user_id
            ,rd.create_date
            ,rd.store_number
            ,rd.style_number
            ,pack.sku_nbr
            ,pack.size_nbr
            ,rd.WL_KEY
            ,rd.result_qty * NVL(pack.qty_per_pack, 1) AS result_qty
            ,CASE WHEN wl.po_nbr = 0 THEN rd.result_qty * NVL(pack.qty_per_pack, 1) ELSE 0 END AS backorder_qty
            ,CASE WHEN wl.po_nbr > 0 THEN rd.result_qty * NVL(pack.qty_per_pack, 1) ELSE 0 END AS po_qty
            ,rd.result_qty AS pack_results
	FROM rd rd
        JOIN aam.worklist wl
        	ON rd.wl_key = wl.wl_key
        JOIN aam.wl_pack pack
       		ON rd.wl_key = pack.wl_key
            AND rd.pack_id = pack.pack_id
    ) ar
    LEFT JOIN aam.RESULTS_STATISTICS rs
    ON ar.allocation_nbr = rs.allocation_nbr
    GROUP BY
        ar.allocation_nbr
        ,ar.user_id
        ,ar.create_date
        ,ar.store_number
        ,ar.style_number
        ,ar.size_nbr
        ,rs.model_key
        ,ar.WL_KEY
) ar
LEFT JOIN (SELECT
        md.model_key
        ,model_name
        ,level_1 AS size_nbr
        ,store AS store_number
        ,SUM(threshold) AS threshold
        ,SUM(target) AS target
    FROM aam.md$import_data md
    JOIN (SELECT DISTINCT rs.allocation_nbr
            ,model_key
            FROM aam.results_statistics rs
            JOIN rd rd
            ON rs.allocation_nbr = rd.allocation_nbr) rs
        ON rs.model_key = md.model_key
    JOIN aam.locations loc
        ON md.location_id = loc.location_id
    GROUP BY     md.model_key
        ,model_name
        ,level_1
        ,store
) md
  ON md.model_key = ar.model_key
  AND md.store_number = ar.store_number
  AND md.size_nbr = ar.size_nbr
 --GROUP BY ar.create_date
--	,ar.store_number
--	,ar.style_number
--	,ar.size_nbr
--	,md.threshold
--	,md.target
--	,ar.model_key