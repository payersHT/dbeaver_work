SELECT
        RCV_ASN.CREATED_BY, 
        RCV_ASN.ORG_ID, 
        RCV_ASN.ASN_ORIGIN_TYPE_ID, 
        RCV_ASN.PRE_RECEIPT_STATUS_ID, 
        RCV_ASN.UPDATED_TIMESTAMP, 
        RCV_ASN.ORIGIN_FACILITY_ID, 
        RCV_ASN.ASN_LEVEL_ID, 
        RCV_ASN.DESTINATION_FACILITY_ID, 
        RCV_ASN.VENDOR_ID, 
        RCV_ASN.SHIPPED_DATE, 
        RCV_ASN.FACILITY_ID, 
        RCV_ASN.UPDATED_BY, 
        RCV_ASN.CONTEXT_ID, 
        RCV_ASN.PROCESS, 
        RCV_ASN.ASN_STATUS, 
        RCV_ASN.CANCELED, 
        RCV_ASN.VERSION, 
        RCV_ASN.PK, 
        RCV_ASN.CREATED_TIMESTAMP, 
        RCV_ASN.TRAILER_ID, 
        RCV_ASN.ESTIMATED_DELIVERY_DATE, 
        RCV_ASN.CARRIER_ID, 
        RCV_ASN.ASN_ID, 
        RCV_ASN.RRN_ID, 
        RCV_ASN.BUSINESS_UNIT_ID, 
        RCV_ASN.PURGE_DATE, 
        RCV_ASN.VERIFICATION_ATTEMPTED, 
        RCV_ASN.SHIPPED_LPNS, 
        RCV_ASN.RECEIVED_LPNS, 
        RCV_ASN.FIRST_RECEIPT_DATE, 
        RCV_ASN.LAST_RECEIPT_DATE, 
        RCV_ASN.EXT_QC_HLD, 
        RCV_ASN.EXT_CASE_SHIPPED, 
        RCV_ASN.EXT_UNITS_SHIPPED, 
        RCV_ASN.EXT_CUST_SEAL_NBR, 
        RCV_ASN.EXT_STS_TRANS_NBR, 
        RCV_ASN.EXT_PREPACK_IND, 
        RCV_ASN.EXT_ORDER_EXISTS, 
        RCV_ASN.EXT_ORDER_CREATED_TIMESTAMP, 
        RCV_ASN.EXT_PO_NUMBER
    FROM
        default_receiving.RCV_ASN RCV_ASN 
    WHERE 
        RCV_ASN.ORG_ID IN ( 
            'HTPC' ) AND
        RCV_ASN.FACILITY_ID IN ( 
            '997', 
            '999' ) AND
        CASE 
            WHEN 
                RCV_ASN.ORG_ID IN ( 
                    'HTPC' ) OR
                RCV_ASN.BUSINESS_UNIT_ID IS NULL
                THEN
                    TRUE
            ELSE RCV_ASN.BUSINESS_UNIT_ID IN ( 
                'NO ACCESS' )
        END