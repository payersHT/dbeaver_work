WITH trans_detail (brand
    ,sku
    ,quantity
    ,transaction_number
    ,transaction_date
    ,SOURCE
    ,FULFILLMENT_STORE
    ,SOLD_PRICE
    ,COGS
    ,LIST_PRICE)
AS 
(SELECT brand_org_code brand
    ,sku
    ,quantity
    ,transaction_number
    ,txn_business_date transaction_date
    ,txn_source_code SOURCE
    ,FULFILLMENT_STORE
    ,SOLD_PRICE
    ,COGS
    ,LIST_PRICE
FROM dm_owner.transaction_detail_mv
WHERE txn_business_date BETWEEN '01-MAY-21' AND '01-JAN-22'
AND line_item_amt_type_cd = 'S'
AND brand_org_code IN ('HT','BL')
AND txn_source_code IN ('HT.Com','BL.Com')
AND is_ship_to_store = '0')
SELECT DISTINCT *
FROM (SELECT prod.sku
        ,td.brand
        ,td.quantity
        ,td.transaction_number
        ,td.transaction_date
        ,td.SOURCE
        ,td.FULFILLMENT_STORE
        ,LIST_PRICE
        ,product_description
        ,level_1_code division
        ,level_2_code group_id
        ,level_2_description group_desc
        ,level_3_code dept 
        ,level_3_description dept_desc 
        ,cs_tran.SHIP_TO_ZIP
		,cs_tran.SHIP_TO_STATE
		,cs_tran.BILL_TO_ZIP
		--,tran_detail.TRANS_LOCATION
        ,level_4_code class
        ,level_4_description class_desc
        ,level_5_code subclass
        ,level_6_description subclass_desc
        ,SOLD_PRICE
        ,COGS
        ,cs_tran.HAS_BACKORDER
        ,cs_tran.HAS_BOPIS
        ,cs_tran.HAS_PRESALE
     --   ,cs_tran.HAS_SHIP_TO_STORE
     --   ,cs_tran.HAS_SHIP_TO_STORE
    FROM trans_detail td
    JOIN dm_owner.product_mv prod
        ON td.sku = prod.sku
    JOIN dm_owner.product_uda_mv a_uda
        ON td.sku = a_uda.item
    JOIN DM_ETL_USER.W_CS_TRANSACTION cs_tran
    	ON td.transaction_number = cs_tran.TRANSACTION_NUMBER);