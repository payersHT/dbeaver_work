SELECT individual_id, 
        b.uda_value_description,
        a.TRANSACTION_NUMBER, 
        a.QUANTITY,
        a.DOLLAR_VALUE_US,
        a.item_number 
FROM dm_owner.transaction_detail_mv a
INNER JOIN dm_owner.product_uda_mv b
    on a.item_number = b.item
where a.brand_org_code = 'HT'
AND a.TRANSACTION_NUMBER ='35014424101'
--and txn_date > sysdate -'14'
--and uda_id in ('28')