SELECT wl.div_nbr,
  Wl.dept_nbr,
  wp.Store_nbr,
  wp.Itm_nbr,
  wp.Sku_Num,
  ':'
  || wp.Size_nbr             AS SIZE_NBR,
  SUM(alloc*NVL(inner_pk,1)) AS Exp_alloc ,
  SUM(Wip  *NVL(Inner_pk,1)) AS EXP_WIP
  /*,sum(Nvl(inner_pk,1)) as Inner_pk*/
FROM
  --Select wl.div_nbr, Wl.dept_nbr, wp.pack_id,wp.Itm_nbr,wp.Sku_Num,wp.Store_nbr,wp.Alloc_nbr,wp.Size_nbr,wp.WL_KEY, Sum(alloc) as alloc ,Sum(Wip) as WIP,sum(inner_pk) as Inner_pk from
  (
  SELECT wp.pack_id,
    wp.Itm_nbr,
    wp.Sku_Num,
    wp.Store_nbr,
    wp.Alloc_nbr,
    wp.Size_nbr,
    Rd.WL_KEY,
    SUM(alloc)    AS alloc ,
    SUM(Wip)      AS WIP,
    SUM(inner_pk) AS Inner_pk
  FROM
    (SELECT wp.Itm_nbr,
      wp.Pack_ID,
      wp.Sku_Num,
      wp.Store_nbr,
      wp.Alloc_nbr,
      pk.Size_nbr,
      SUM(alloc)    AS alloc ,
      SUM(Wip)      AS WIP,
      SUM(inner_pk) AS Inner_pk
    FROM
      (SELECT NVL(rms.itm_nbr,wp.itm_nbr) AS itm_nbr ,
        wp.sku_num                        AS Pack_Id,
        wp.Store_nbr,
        wp.alloc_nbr,
        NVL(RMS.Sku_number,wp.sku_num) AS Sku_num ,
        SUM(wp.alloc_qty)              AS alloc,
        SUM(wp.WIP_OH_QTY)             AS WIP,
        SUM(Qty_per_pack)              AS inner_pk
      FROM aam.HT_WIP_STAGE WP
      LEFT OUTER JOIN aam.HT_Base_rms_packs RMS
      ON RMS.pack_id = wp.sku_num
      GROUP BY NVL(rms.itm_nbr,wp.itm_nbr),
        wp.sku_num,
        wp.Store_nbr,
        wp.alloc_nbr,
        NVL(RMS.Sku_number,wp.sku_num)
      ) wp
    LEFT OUTER JOIN
      (SELECT sku_nbr, Size_nbr FROM aam.WL_pack GROUP BY sku_nbr, Size_nbr
      ) Pk
    ON wp.sku_num = pk.sku_nbr
    GROUP BY wp.itm_nbr,
      wp.pack_ID,
      wp.Sku_Num,
      wp.Store_nbr,
      wp.Alloc_nbr,
      pk.Size_nbr,
      wp.sku_num
    ) wp
  LEFT OUTER JOIN aam.Results_Detail rd
  ON rd.allocation_nbr  = wp.alloc_nbr
  AND rd.location_id    = wp.store_nbr
  AND rd.unique_mer_key = wp.itm_nbr
    || '.'
    || wp.pack_id
  GROUP BY wp.pack_id,
    wp.itm_nbr,
    wp.Sku_Num,
    wp.Store_nbr,
    wp.Alloc_nbr,
    wp.Size_nbr,
    Rd.WL_KEY
  ) wp
LEFT OUTER JOIN aam.Worklist wl
ON wp.WL_KEY            = wl.WL_Key
WHERE wl.po_cancel_date &gt; sysdate - 14
AND WIP                != 0
AND wl.div_nbr         =:Division
  --group by wl.div_nbr, Wl.dept_nbr, wp.pack_id,wp.Itm_nbr,wp.Sku_Num,wp.Store_nbr,wp.Alloc_nbr,wp.Size_nbr,wp.WL_KEY
GROUP BY wl.div_nbr,
  Wl.dept_nbr,
  wp.Store_nbr,
  wp.Itm_nbr,
  wp.Sku_Num,
  wp.Size_nbr