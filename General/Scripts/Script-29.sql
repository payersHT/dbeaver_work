WITH rd AS (SELECT rh.user_id
            ,TRUNC(rh.createdate) AS create_date
            ,rd.allocation_nbr
            ,loc.LOCATION_ID AS store_number
            ,wl.itm_nbr
            ,SUBSTR (rd.UNIQUE_MER_KEY, 1, INSTR(rd.UNIQUE_MER_KEY, '.', -1, 1) -1) AS style_number
            ,SUBSTR (rd.UNIQUE_MER_KEY, INSTR(rd.UNIQUE_MER_KEY, '.', -1, 1) +1) AS sku_number
            ,rd.wl_key
            ,rd.result_qty
            FROM aam.RESULTS_DETAIL rd
            JOIN aam.results_header rh
            ON rd.allocation_nbr = rh.allocation_nbr
            JOIN aam.worklist wl
            ON rd.wl_key = wl.wl_key
            JOIN aam.locations loc
            ON rd.location_id = loc.store
            WHERE TRUNC(rh.createdate)  BETWEEN TRUNC(SYSDATE - 25) AND TRUNC(SYSDATE - 1) 
            AND wl.div_nbr IN ('1','8')
            AND wl.PO_NBR > 0
            AND wl.STATUS_CODE = 40
)
SELECT rd.ALLOCATION_NBR
            ,rd.user_id
            ,rd.create_date
            ,rd.store_number
            ,rd.itm_nbr
            ,rd.style_number
            ,rd.sku_number
            ,pack.size_nbr
            ,rd.WL_KEY
            ,rd.result_qty * NVL(pack.qty_per_pack, 1) AS result_qty
            ,rd.result_qty AS pack_results
	FROM rd rd
        JOIN aam.worklist wl
        	ON rd.wl_key = wl.wl_key
        JOIN aam.wl_pack pack
       		ON rd.wl_key = pack.wl_key
            AND rd.sku_number = pack.sku_nbr
            WHERE rd.store_number NOT IN (9999,9997)