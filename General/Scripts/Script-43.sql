SELECT DISTINCT individual_id,
                min_txn_date,
                case when count(distinct channel) = 1 then 'only' || max(channel)
                else 'cross'
                end as groups,
                SUM (dollar_value_us)             dollar_value_us,
                SUM (quantity)                    sum_of_quantity,
                COUNT (DISTINCT transaction_number) count_of_transactions, ---added distinct
                CASE WHEN MIN (TRUNC (txn_date)) = min_txn_date THEN 1 ELSE 0 END
                    is_new
            FROM (SELECT individual_id,
                case when txn_source_code in ('POS','Kiosk','STS') then 'store'
                when txn_source_code not in ('POS','Kiosk','STS') then 'ecom' end as channel,
                        dollar_value_us,
                        txn_date,
                        quantity,
                        transaction_number,
                        MIN (TRUNC (txn_date)) OVER (PARTITION BY individual_id)
                            min_txn_date
                    FROM DM_OWNER.transaction_detail_mv
                WHERE     brand_org_code = 'HT'
                        AND is_merch = 1
                        AND currency_code = 'USD'
                        AND line_item_amt_type_cd = 'S')
        WHERE TRUNC (txn_date) BETWEEN '01-JAN-20' AND '30-JAN-22'
        and INDIVIDUAL_ID is not null --added this as we had 1 record off for those txns we didnt have an INDIVIDUAL_ID for on the txn table (these are Cash txns)
        GROUP BY individual_id, min_txn_date