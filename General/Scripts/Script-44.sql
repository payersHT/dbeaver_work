SELECT L_rn,
L_CNT,
License,
L_TRANS, 
L_UNITS, 
L_SALES, 
L_MARGIN, 
(L_MARGIN/L_CNT) AS LMarginPerCust
FROM (SELECT L_rn,
            L_CNT,
            License, 
            L_TRANS, 
            L_UNITS, 
            L_SALES, 
            L_MARGIN from (select 
                            RANK() OVER(ORDER BY count(distinct individual_id) desc) AS L_rn,
                            count(distinct individual_id) AS L_CNT, 
                            uda_value_description AS License,
                            count (distinct TRANSACTION_NUMBER) AS L_TRANS, 
                            sum(a.QUANTITY) AS L_UNITS,
                            sum(a.DOLLAR_VALUE_US) AS L_SALES,
                            sum(a.DOLLAR_VALUE_US) - (sum(a.COGS*a.quantity)) AS L_MARGIN
                            from dm_owner.transaction_detail_mv a
                            join dm_owner.product_uda_mv b
                                on a.item_number = b.item
                            where a.brand_org_code = 'HT'
                            and txn_date > sysdate -'365'
                            and uda_id in ('28') --28 = LICENSE --33 = BAND   
--and txn_source_code NOT in ('POS', 'STS', 'Kiosk') --Web or Store
                            group by uda_value_description
                            order by count(distinct individual_id) desc)
        WHERE L_RN <= '30') a
        
        
        
        JOIN (SELECT B_rn,
                B_CNT,BAND, 
                B_TRANS, 
                B_UNITS, 
                B_SALES, 
                B_MARGIN from (select  RANK() OVER(ORDER BY count(distinct individual_id) desc) AS B_rn,
                                count(distinct individual_id) as B_CNT, 
                                uda_value_description as BAND,
                                count (distinct TRANSACTION_NUMBER) as B_TRANS, 
                                sum(a.QUANTITY) as B_UNITS,
                                sum(a.DOLLAR_VALUE_US) as B_SALES,
                                sum(a.DOLLAR_VALUE_US) - (sum(a.COGS*a.quantity)) as B_MARGIN
                                from dm_owner.transaction_detail_mv a
                                join dm_owner.product_uda_mv b
                                on a.item_number = b.item
                                where a.brand_org_code = 'HT'
                                and txn_date > sysdate-'365'
                                and uda_id in ('33') --28 = LICENSE --33 = BAND   
--and txn_source_code NOT in ('POS', 'STS', 'Kiosk') --Web or Store
                                group by uda_value_description
                                order by count(distinct individual_id) desc)
                WHERE B_RN <= '30') b
                on L_rn = B_rn;