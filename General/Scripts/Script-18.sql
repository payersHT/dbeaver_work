select 
min(date_format(CONVERT_TZ(RCPT.CREATED_TIMESTAMP,  'UTC', 'US/Pacific'), '%m/%d/%Y')) "Rcpt. Date (Pacific)",
POL.PURCHASE_ORDER_ID "PO",  
POL.ITEM_ID as 'itm', 
POL.ORDER_QUANTITY "Ord. Qty",
RCPT.ASN_ID "ASN",
coalesce(sum(RCPT.QUANTITY), 0.0000) "Rcpt. Qty",
case when POL.CANCELED = 1 then 'Yes' else 'No' end "Cancelled?"
from default_receiving.RCV_PURCHASE_ORDER_LINE POL
left outer join default_receiving.RCV_RECEIPT RCPT 
	on POL.PURCHASE_ORDER_ID = RCPT.PURCHASE_ORDER_ID 
	AND POL.ITEM_ID = RCPT.ITEM_ID
where POL.PURCHASE_ORDER_ID IN ( select DISTINCT (POLPO.PURCHASE_ORDER_ID)
					              from default_receiving.RCV_PURCHASE_ORDER_LINE POLPO
					              /*We will need to pass in a list of strings into the POLPO.ITEM. */
					              ) 
and POL.PURCHASE_ORDER_ID  in ('1895489')
GROUP BY POL.PURCHASE_ORDER_ID, 
POL.CANCELED,
POL.ITEM_ID, 
POL.ORDER_QUANTITY, 
/*date_format(CONVERT_TZ(RCPT.CREATED_TIMESTAMP,  'UTC', 'US/Pacific'), '%m/%d/%Y'),*/
RCPT.ASN_ID
order by POL.PURCHASE_ORDER_ID, POL.ITEM_ID, 1


