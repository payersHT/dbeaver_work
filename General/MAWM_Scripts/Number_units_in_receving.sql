SELECT *
FROM (SELECT item_id,
CREATED_TIMESTAMP ,
allocation_source_id,
original_order_id,
SUBSTRING_INDEX(SUBSTRING_INDEX(original_order_id,'_',2),'_',-1) as store_number,
SUBSTRING_INDEX(original_order_id,'_',-1) as allocation_line_number, 
description,
shipped_quantity,
STATUS,
ALLOCATED_QUANTITY,
EXT_DISTRO_NBR,
EXT_PONBR 
FROM default_dcorder.DCO_ORDER_LINE dol ) as pip
where pip.store_number in ('4490','4499')
AND EXT_PONBR is NULL 