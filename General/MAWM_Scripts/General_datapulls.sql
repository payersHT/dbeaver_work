
--We can use the RCV_LPN to see all of the ASN that are attached to a PO, and what status the ASN is in.
default_receiving.RCV_LPN -- THEN FILTER ON PO --


--We can get the LPN Detail Status for each PO
default_receiving.RCV_LPN_DETAIL -- then filter on PO number --

--We can get Trailer ID adn Carrier ID for each ASN ID. 
default_yeard_management.YRD_ASN --filter on ASN


